import java.awt.List;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

import javax.lang.model.element.Element;

public class ReadText {
	private static double elementList[];
	private static double uniqueList[];
	private static int indexes[];
	
	public static void main(String[] args) {
		prompt();
	}
	private static void prompt() {
		readFile();
		// print the frequency of all
		printFirstOutput();
		System.out.println("********************************************************************");
		System.out.println("[1]Print Given Frequency [2]Print Output For Unique Numbers");
		
		int x = new Scanner(System.in).nextInt();
		switch (x) {
		case 1:
			printSecondOutput();
			break;
		case 2:
			printUniqueOutput();
			break;
		default:
			System.out.println("Invalid Input");
			break;
		}
	}
	
	private static void readFile() {
		try {
			
			FileReader fl = new FileReader("STUDGRADE.txt");
			BufferedReader br = new BufferedReader(fl);
			elementList = new double[GetSize()];
			String line;
			int x=0;
			//get the current size
			while ((line=br.readLine())!=null) {
				elementList[x] = Double.parseDouble(line);
				x++;	
			}
		
		
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	private static int GetSize() throws NumberFormatException, IOException {
		FileReader fl = new FileReader("STUDGRADE.txt");
		BufferedReader br = new BufferedReader(fl);
		String line;
		int x=0;
		//get the current size
		while ((line=br.readLine())!=null) {
		
			x++;	
		}
		return x;
	}
	private static void printAllElements() {
		for(int x=0;x<elementList.length;x++) {
			System.out.println(elementList[x]);
		}
	}
	private static void printFirstOutput() {
		//Get Average
		double temp=0;
		for(int x=0;x<elementList.length;x++) {
			temp+=elementList[x];
		}
		
		double mean =  temp/elementList.length;
		System.out.println("Total: " + temp);
		System.out.println("Frequency: " +elementList.length);
		System.out.println("Average/Mean: " +mean);
		System.out.println("Standard Deviation: " + getStandardDeviation(elementList, mean));
	}
	
	private static double getStandardDeviation(List l, double mean) {
		double powSum1=0,powSum2=0;
		for(int x = 0;x<l.getItemCount();x++) {
			 powSum1+=Double.parseDouble(l.getItem(x));
			 powSum2+=Math.pow(Double.parseDouble(l.getItem(x)), 2);
		}
		return Math.sqrt(l.getItemCount()*powSum2 - Math.pow(powSum1, 2))/l.getItemCount();
	}
	
	private static double getStandardDeviation(double l[], double mean) {
		double powSum1=0,powSum2=0;
		for(int x = 0;x<l.length;x++) {
			 powSum1+=Double.parseDouble(String.valueOf(l[x]));
			 powSum2+=Math.pow(Double.parseDouble(String.valueOf(l[x])), 2);
		}
		return Math.sqrt(l.length*powSum2 - Math.pow(powSum1, 2))/l.length;
	}
	
	private static void printSecondOutput() {
		System.out.print("Enter number to randomize: ");
		try {
		int x = new Scanner(System.in).nextInt();
		if(x > elementList.length) {
			System.out.println("Number of elements are less than the input");
		}
		else {
			double arr[] = generateRandoms(x);
			//Get Average
			double total=0;
			for(double element:arr) {
				total+=element;
			}
			System.out.println("Total: " + total);
			System.out.println("Frequency: " +arr.length);
			System.out.println("Average/Mean: " +total/arr.length);
			System.out.println("Standard Deviation: " + getStandardDeviation(arr, total/arr.length));
			System.out.println("Indexes: ");
			for(int z:indexes) {
				System.out.println("Line "+(z+1) + "   : Element: " + elementList[z]);
			}
		}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	private static double[] generateRandoms(int amount) {
		indexes = new int[amount];
		double arr[] = new double[amount];
		int x=0;
		while(x<amount) {
			int temp = new Random().nextInt((elementList.length-1)+1)-0;
			indexes[x] = temp;
			while(repeatedIndex(temp)) {
				temp = new Random().nextInt((elementList.length-1)+1)-0;
			}
			arr[x] = (int) elementList[temp];
			x++;
		}
		return arr;
	}
	private static boolean repeatedIndex(int index) {
		boolean x=false;
		for(int y:indexes) {
			if(index==y) {
				x=true;
			}
		}
		return x;
	}
	
	private static double[] getAllUnique() {
		uniqueList = new double[getNumberOfUniqueItems()];
		indexes = new int[getNumberOfUniqueItems()];
		int numElements = elementList.length;
		int ctr=0;
		try {	
		for(int x=0;x<numElements;x++) {
			int y=0;
			for(int z=0;z<numElements;z++) {
				if(elementList[x]==elementList[z]) {
					y++;
				}
			}
			if(y==1) {
				uniqueList[ctr] = elementList[x];	
				indexes[ctr]=x;
				ctr++;
			}
			
		}

		}catch (Exception e) {
			e.printStackTrace();
		}
		return uniqueList;
	}
	private static int getNumberOfUniqueItems() {
		int unique=0;
		for(int x=0;x<elementList.length;x++) {
			int y=0;
			for(int z=0;z<elementList.length;z++) {
				if(elementList[x]==elementList[z]) {
					y++;
				}
			}
			if(y==1) {
				unique++;
			}
		}
		return unique;
	}
	
	private static void printUniqueOutput() {
		double list[] = getAllUnique();
		double temp=0;
		for(int x=0;x<list.length;x++) {
			temp+=list[x];
		}
		
		double mean =  temp/list.length;
		System.out.println("Total: " + temp);
		System.out.println("Frequency: " +list.length);
		System.out.println("Average/Mean: " +mean);
		System.out.println("Standard Deviation: " + getStandardDeviation(list, mean));
		System.out.println("Unique Elements: ");
		for(int index:indexes) {
			System.out.println("Line number: "+(index+1) + " Element: " + elementList[index] );
		}
	}

}
